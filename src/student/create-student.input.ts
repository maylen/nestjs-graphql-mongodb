import  { MinLength, IsNotEmpty } from 'class-validator';
import { InputType, Field } from '@nestjs/graphql'

@InputType()
export class CreateStudentInput {

    @MinLength(1)
    @IsNotEmpty()
    @Field()
    firstName: string;

    @MinLength(1)
    @IsNotEmpty()
    @Field()
    lastName: string;
}