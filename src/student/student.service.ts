import { Injectable } from '@nestjs/common';
import { Student } from './student.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateStudentInput } from './create-student.input';
import { v4 as uuid } from 'uuid';

@Injectable()
export class StudentService {

  constructor(@InjectRepository(Student) private studentRepository: Repository<Student>,
  ) {}
    
  async getStudent(id: string): Promise<Student> {
        /**
        use curly brackets around id - otherwise mongo uses mongo ID to
        search lesson - so we are here using public id
        */
      return this.studentRepository.findOne({ id });
  }

  async getAllStudents(): Promise<Student[]> {
      return this.studentRepository.find();
  }

  async createStudent(createStudentInput: CreateStudentInput): Promise<Student> {
    const { firstName, lastName } = createStudentInput;
    const student = this.studentRepository.create({
            id: uuid(),
            firstName,
            lastName
        });
    return this.studentRepository.save(student);
  }


  async getManyStudents(studentsIds: string[]): Promise<Student[]> {

      return this.studentRepository.find({
            where: {
                id: {
                    $in: studentsIds,
                }
            }
      })
  }

}
