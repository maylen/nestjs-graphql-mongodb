/* eslint-disable @typescript-eslint/no-unused-vars */

import { Resolver, Args, Query } from '@nestjs/graphql';
import { Mutation } from '@nestjs/graphql';
import { Student } from './student.entity';
import { CreateStudentInput } from './create-student.input';
import { StudentType } from './student.type';
import { StudentService } from './student.service';

@Resolver(of => StudentType)
export class StudentResolver {

    constructor(private studentService: StudentService) {}

    @Query(returns => StudentType)
    async getStudent(@Args('id') id: string): Promise<Student>  {
        return this.studentService.getStudent(id);
    }

    @Query(returns => [StudentType])
    async getAllStudents(): Promise<Student[]> {
        return this.studentService.getAllStudents();
    }

    @Mutation(returns => StudentType)
    async createStudent(
        @Args('createStudentInput') createStudentInput: CreateStudentInput
        ): Promise<Student> {
            
        return this.studentService.createStudent(createStudentInput);
    }

}