import { InputType, Field, ID } from '@nestjs/graphql';
import { IsUUID } from 'class-validator'; 

@InputType()
export class AssignStudentsToLessonInput {

    @IsUUID()
    // type = GraphQl's internal/private ID 
    @Field(type => ID)
    lessonId: string;
    
    @IsUUID("4", { each: true })
    // type = GraphQl's internal/private ID array
    @Field(type => [ID])
    studentsIds: string[];

} 