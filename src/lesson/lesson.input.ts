import { InputType, Field, ID } from '@nestjs/graphql';
import { MinLength, IsDateString, IsUUID } from 'class-validator';


// this is the DTO for lesson input

@InputType() //defines GraphQl input
export class CreateLessonInput {
    @MinLength(1)
    @Field()
    name: string;

    @IsDateString()
    @Field()
    startDate: string;

    @IsDateString()
    @Field()
    endDate: string;

    @IsUUID("4", { each: true })
    // here we nust provide also empty array as defaultvalue
    @Field(() => [ID], { defaultValue: [] })
    students: string[];
}
