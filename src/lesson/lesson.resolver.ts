/* eslint-disable @typescript-eslint/no-unused-vars */
// is the same that controller is in restful apps

import { Resolver, Query, Mutation, Args, ResolveField, Parent } from '@nestjs/graphql';
import { LessonType } from './lesson.type';
import { LessonService } from './lesson.service';
import { Lesson } from './lesson.entity';
import { CreateLessonInput } from './lesson.input';
import { AssignStudentsToLessonInput } from './assign-students-to-lesson.input';
import { StudentService } from './../student/student.service';

// this is resolver for the type LessonType
@Resolver(of => LessonType)
export class LessonResolver {
  constructor(
    private lessonService: LessonService,
    private studentService: StudentService
    ) {}

  @Query(returns => LessonType)
  lesson(@Args('id') id: string): Promise<Lesson> {
    return this.lessonService.getLesson(id);
  }

  @Query(returns => [LessonType])
  allLessons(): Promise<Lesson[]> {
    return this.lessonService.getAllLessons();
  }

  @Mutation(returns => LessonType)
  async createLesson(
    @Args('createLessonInput') createLessonInput: CreateLessonInput,
  ): Promise<Lesson> {
    return this.lessonService.createLesson(createLessonInput);
  }


  @Mutation(returns => LessonType)
  assignStudentsToLesson(
      @Args('assignStudentsToLessonInput') assignStudentsToLessonInput: 
      AssignStudentsToLessonInput): Promise<Lesson> {
        const { lessonId, studentsIds} = assignStudentsToLessonInput;
        
        return this.lessonService.assignStudentsToLesson(lessonId, studentsIds);

  }

  @ResolveField()
  async students(@Parent() lesson: Lesson): Promise<any> {
    // console.log(lesson);
    return this.studentService.getManyStudents(lesson.students);
  }

}
