import { Entity, PrimaryColumn, Column, ObjectIdColumn } from 'typeorm';

@Entity()
export class Lesson {
    // Mongo the DB expects objects to have an underscoreID property by default.
    // This is is the internal object id for mongo  object ID
    @ObjectIdColumn()
    _id: string

    @PrimaryColumn()
    id: string;

    @Column()
    name: string;

    @Column()
    startDate: string;

    @Column()
    endDate: string;

    @Column()
    students: string[];
}

