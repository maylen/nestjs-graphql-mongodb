import { ObjectType, Field, ID } from '@nestjs/graphql';
import { StudentType } from './../student/student.type';
/**
 GraphQl has types in it schemas and it senses the types of the properties 
 of the object from typescript types. ID field is exception for this and 
 name it given spesifically with the GraphQl type ID (with capitalized letters).
 */
@ObjectType('Lesson')
export class LessonType {

    @Field(type => ID)
    id: string;

    @Field()
    name: string;
    
    @Field()
    startDate: string;
    
    @Field()
    endDate: string;

    @Field(type => [StudentType])
    students: string[];
}