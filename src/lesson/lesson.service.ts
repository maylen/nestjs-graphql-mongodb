import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Lesson } from './lesson.entity';
import { v4 as uuid} from 'uuid';
import { CreateLessonInput } from './lesson.input';

@Injectable()
export class LessonService {

    constructor(@InjectRepository(Lesson) private lessonRepository: Repository<Lesson>) {
        
    }

    async getLesson(id: string): Promise<Lesson> {
        /**
            use curly brackets around id - otherwise mongo uses mongo ID to
            search lesson - so we are here using public id
        */
        return this.lessonRepository.findOne({ id });
    }


    async getAllLessons(): Promise<Lesson[]> {
        // use curly brackets otherwise mongo uses mongo ID to search lesson
        // so we are here using just id (i.e. custom / public id)
        return this.lessonRepository.find();
    }


    // here we again use dto
    async createLesson(createLessonInput: CreateLessonInput ): Promise<Lesson> {

        const { name, startDate, endDate, students } = createLessonInput;

        const lesson = this.lessonRepository.create({
            id: uuid(),
            name,
            startDate,
            endDate,
            students
        });
        // OR
        // const lesson = this.lessonRepository.create({
        //     id: uuid(),
        //     name: name,
        //     startDate: startDate,
        //     endDate: endDate 
        // });
        // OR
        // const lesson = this.lessonRepository.create();
        // lesson.id = uuid();
        // lesson.name = name;
        // lesson.startDate = startDate,
        // lesson.endDate = endDate 
    
        return this.lessonRepository.save(lesson);
    }

    async assignStudentsToLesson(lessonId: string, studentIds: string[]): Promise<Lesson> {
        const lesson = await this.lessonRepository.findOne({id: lessonId});
        lesson.students = [...lesson.students, ...studentIds];
        return this.lessonRepository.save(lesson);
    }
}
