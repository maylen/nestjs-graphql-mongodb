import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // here we declare the usage of pipes globally 
  // (i.e. available and usable in all modules)
  app.useGlobalPipes(new ValidationPipe());
  await app.listen(3000);
}
bootstrap();
